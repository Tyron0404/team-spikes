package sender;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by tyron.surajpal on 2017/07/05.
 */
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);

        ApplicationContext context = new GenericXmlApplicationContext("classpath:/app-context.xml");
        AmqpTemplate template = context.getBean(AmqpTemplate.class);
        ObjectMapper mapper = new ObjectMapper();
        FileInformation info = new FileInformation();
        info.FileName = "test.csv";
        info.FilePath = "C:\\Users\\tyron.surajpal\\Desktop";

        try {
            String jsonString = mapper.writeValueAsString(info);
            while (true) {
                Scanner scan = new Scanner(System.in);
                System.out.println("Send message(y/n) ?");
                String reply = scan.nextLine();
                if (reply.equals("y")) {
                    template.convertAndSend("myqueue", jsonString);
                } else {
                    System.exit(0);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
