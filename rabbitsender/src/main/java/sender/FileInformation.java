package sender;

        import java.io.*;

/**
 * Created by tyron.surajpal on 2017/07/05.
 */
public class FileInformation implements Serializable {
    public String FileName;
    public String FilePath;

    public String getFullPath() {
        return FilePath + "\\" + FileName;
    }

    public static byte[] serialize(Object obj) throws IOException {
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            try {
                ObjectOutputStream o = new ObjectOutputStream(b);
                o.writeObject(obj);
            } catch (Exception ex) {

            }
            return b.toByteArray();
        } catch (Exception ex) {

        }
        return null;
    }

    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException, Exception {
        ByteArrayInputStream b = new ByteArrayInputStream(bytes);

        ObjectInputStream o = new ObjectInputStream(b);
        return o.readObject();
    }
}
