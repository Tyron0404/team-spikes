package batch;

import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import models.Person;

/**
 * Created by tyron.surajpal on 2017/07/05.
 */
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;



    /*@Autowired
    public DataSource dataSource;*/

    // tag::readerwriterprocessor[]
    @StepScope
    @Bean
    public FlatFileItemReader<Person> reader(@Value("#{jobParameters['FilePath']}") String FilePath) {
        System.out.println("Creating reader...");
        FlatFileItemReader<Person> reader = new FlatFileItemReader<Person>();
        reader.setResource(new FileSystemResource(FilePath));
        reader.setLineMapper(new DefaultLineMapper<Person>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[]{"firstName", "lastName"});
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                setTargetType(Person.class);
            }});
        }});
        return reader;
    }

    @Bean
    public ItemProcessor<Person, Person> processor() {
        System.out.println("Creating processor...");
        return new PersonItemProcessor();
    }

    /*    @Bean
        public ItemWriter<Person> writer(DataSource dataSource) {
            System.out.println("Creating writer...");
            JdbcBatchItemWriter<Person> writer = new JdbcBatchItemWriter<Person>();
            writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Person>());
            writer.setSql("INSERT INTO people (first_name, last_name) VALUES (:firstName, :lastName)");
            writer.setDataSource(dataSource);
            return writer;
        }*/
    @Bean
    public FlatFileItemWriter<Person> writer() {
        FlatFileItemWriter<Person> writer = new FlatFileItemWriter<>();
        writer.setResource(new FileSystemResource("src/persons.txt"));
        writer.setLineAggregator(new DelimitedLineAggregator() {{
            setFieldExtractor(new BeanWrapperFieldExtractor() {{
                setNames(new String[]{"firstName", "lastName"});
            }});
        }});
        return writer;
    }
/*    @Bean
    public StaxEventItemWriter<Person> writer() {
        StaxEventItemWriter<Person> xmlFileWriter = new StaxEventItemWriter<>();

        String exportFilePath = "C:\\Users\\tyron.surajpal\\Desktop\\persons.xml";
        xmlFileWriter.setResource(new FileSystemResource(exportFilePath));

        xmlFileWriter.setRootTagName("person");

        Jaxb2Marshaller studentMarshaller = new Jaxb2Marshaller();
        studentMarshaller.setClassesToBeBound(Person.class);
        xmlFileWriter.setMarshaller(studentMarshaller);

        return xmlFileWriter;
    }*/

    // end::readerwriterprocessor[]

    @Bean
    public JobExecutionListener listener() {
        return new JobCompletionNotificationListener();
    }

    // tag::jobstep[]
    @Bean
    public Job importUserJob(final Step step1, JobExecutionListener listener) {
        System.out.println("Importing job...");

        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(final ItemReader<Person> reader,
                      final ItemWriter<Person> writer, final ItemProcessor<Person, Person> processor) {
        System.out.println("Building step...");
        return stepBuilderFactory.get("step1")
                .<Person, Person>chunk(10)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }

    // end::jobstep[]
}

