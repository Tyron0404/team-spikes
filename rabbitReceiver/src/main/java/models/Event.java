package models;

/**
 * Created by tyron.surajpal on 2017/07/14.
 */
public enum Event {
    FileReceived,
    FileValidated,
    FileReadyForProcessing,
}
