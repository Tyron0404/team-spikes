package models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * Created by tyron.surajpal on 2017/07/05.
 */
public class FileInformation implements Serializable {
    public String FileName;
    public String FilePath;

    @JsonIgnore
    public String getFullPath() {
        return FilePath + "\\" + FileName;
    }
}

