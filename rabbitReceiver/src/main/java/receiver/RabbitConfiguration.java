package receiver;

import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import receiver.QueueReader;

/**
 * Created by tyron.surajpal on 2017/07/07.
 */
@Configuration
public class RabbitConfiguration {

    @Autowired
    public ConnectionFactory connectionFactory;

    @Bean
    public Queue getQueue() {
        return new Queue("myqueue");
    }

    @Bean
    public RabbitTemplate getTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setRoutingKey("myqueue");
        template.setConnectionFactory(connectionFactory);
        return template;
    }

    @Bean
    public RabbitAdmin getAdmin() {
        RabbitAdmin admin = new RabbitAdmin(connectionFactory);
        return admin;
    }

    @Bean
    public MessageListener queueReader() {
        return new QueueReader();
    }

    @Bean
    SimpleMessageListenerContainer container() {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(getQueue().getName());
        container.setMessageListener(queueReader());
        return container;
    }

}
