package receiver;

import batch.BatchConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.QueueEvent;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;

import javax.sql.DataSource;

/**
 * Created by tyron.surajpal on 2017/07/06.
 */
@Import(BatchConfiguration.class)
public class QueueReader implements MessageListener {

    @Autowired
    public DataSource dataSource;

    @Override
    public void onMessage(Message message) {
        System.out.println("Reading from queue...");
        ObjectMapper mapper = new ObjectMapper();
        try {
            // byte[] foo = (byte[]) template.receiveAndConvert("myqueue");
            byte[] foo = message.getBody();

            String jsonString = new String(foo);
            //FileInformation fileInfo = mapper.readValue(jsonString, FileInformation.class);
            QueueEvent event = mapper.readValue(jsonString, QueueEvent.class);
            JobParameters jobParameters = new JobParametersBuilder()
                    .addString("FilePath", event.FilePath).toJobParameters();
            ApplicationContext context = new AnnotationConfigApplicationContext(BatchConfiguration.class);
            JobLauncher launcher = (JobLauncher) context.getBean("jobLauncher");
            Job importUserJob = (Job) context.getBean("importUserJob");
            JobExecution jobExecution = launcher.run(importUserJob, jobParameters);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
